const express =require('express')
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors')
const port = process.env.PORT || 4000;
const swaggerUI = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');

app.use(cors())
app.use(bodyParser.json());

const swaggerOption = {
    swaggerDefinition:{
        openapi: "3.0.0",
        info:{
            title:"Device OAS3.0",
            description: "Device List Api",
            version: "2.0.0",
            Servers: ['http://localhost:4000/']
        }
        
    },
    apis: ["index.js" , "./Router/index.js"]
}

const swaggerDocs = swaggerJSDoc(swaggerOption);
app.use('/api-docs',swaggerUI.serve,swaggerUI.setup(swaggerDocs));

//routes
/**
 * @swagger
 *      components:   
 *          schemas:
 *              devices:
 *                  type: object
 *                  required:
 *                      - DeviceName
 *                      - Model
 *                  properties:
 *                      id:
 *                          type: integer
 *                      DeviceName:
 *                          type: String
 *                      Model:
 *                          type: String
 *                  example:
 *                      DeviceName: Lenovo
 *                      Model: A13
 * @swagger
 * paths:
 *  /devices:
 *      get:
 *          description: GET Request
 *          responses:
 *              200:
 *                  description: Response Prompted
 *                  content:
 *                      application/json:
 *                          schema:
 *                              $ref: '#/components/schemas/devices'
 *                      application/xml:
 *                          schema:
 *                              $ref: '#/components/schemas/devices'
 *                              
 *      post:
 *          responses:
 *              200:
 *                  description: Response Prompted
 *                  content:
 *                      application/json:
 *                          schema:
 *                              $ref: '#/components/schemas/devices'
 *                      application/xml:
 *                          schema:
 *                              $ref: '#/components/schemas/devices'
 * 
 *      put:
 *          responses:
 *              200:
 *                  description: Response Prompted
 *                  content:
 *                      application/json:
 *                          schema:
 *                              $ref: '#/components/schemas/devices'
 *                      application/xml:
 *                          schema:
 *                              $ref: '#/components/schemas/devices'
 *      delete:
 *          responses:
 *              200:
 *                  description: Response Prompted
 *                  content:
 *                      application/json:
 *                          schema:
 *                              $ref: '#/components/schemas/devices'
 *                      application/xml:
 *                          schema:
 *                              $ref: '#/components/schemas/devices'
 *  /active/models:
 *      get:
 *          responses:
 *              200:
 *                  description: Response Prompted
 *                  content:
 *                      application/json:
 *                          schema:
 *                              $ref: '#/components/schemas/devices'
 *                      application/xml:
 *                          schema:
 *                              $ref: '#/components/schemas/devices'
 *          
 */
const DeviceList = require('./Router/index');

app.use('/active', DeviceList);

app.get('/',(req,res)=>{
    res.end("nodejs running")
})

app.get('/devices',(req,res)=>{
    res.end("Device list is printed");
})

app.post('/devices',(req,res)=>{
    res.end("New Device Added");
})

app.put('/devices',(req,res)=>{
    res.end("Device list Updated");
})

app.delete('/devices',(req,res)=>{
    res.end("Device is Deleted");
})



app.listen(port,()=>{
    console.log("Server is ready")
})